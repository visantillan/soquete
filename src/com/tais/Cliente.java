package com.tais;

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.net.*;
import java.io.*;

public class Cliente extends JFrame implements ActionListener
{

    Container c;
    JTextField op1, op2;
    JLabel l1, l2, l3;
    JPanel pN, pC, pS;
    JPanel p1, p2, p3, p4;
    JButton boton;
    JTextArea display;

    public Cliente()
    {
        setTitle("Cliente de socket");
        setLayout(new GridLayout(3, 1));
        c = getContentPane();
        c.setBackground(Color.GRAY);
        // creación de las intancias y agregación a los paneles
        pN = new JPanel();
        pC = new JPanel();
        pS = new JPanel();
        op1 = new JTextField(20);
        op2 = new JTextField(20);        
        l1 = new JLabel("Introduce los valores para:");
        l2 = new JLabel("Primer sumado");
        l3 = new JLabel("Segundo sumando");
        p1 = new JPanel();
        p2 = new JPanel();
        p3 = new JPanel();
        p4 = new JPanel();
        boton = new JButton("Servidor has la suma.");
        display = new JTextArea(30, 30);
        c.add(pN);
        c.add(pC);
        c.add(pS);
        pN.add(l1);
        pC.add(l2);
        pC.add(op1);
        pC.add(l3);
        pC.add(op2);
        pC.add(boton);
        pS.add(display);

        // de las componentes GUI
        pack();
        this.setLocationRelativeTo(null);
        setVisible(true);
        setSize(400, 400);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        boton.addActionListener(this);

    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        double s1, s2;
        s1 = Double.parseDouble(op1.getText());
        s2 = Double.parseDouble(op2.getText());
        System.out.println("Haciendo petición");
        peticionServidor(s1, s2);
    }

    private void peticionServidor(double s1, double s2)
    {
        // declaración de un objecto para el socket cliente
        Socket client;
        Socket ip = null;
        DataInputStream input;
        DataOutputStream output;
        InetAddress direccion;

        // declaración de los objetos para el flujo de datos
        double suma;
        String Suma;
        try
        {
            String ipServer = "192.168.175.86";
            System.out.println(ipServer);
            client = new Socket(InetAddress.getByName(ipServer), 6000);
            //CONTRUCTOR QUE REGRESA IP Y EL PUERTO logico"socket"
            // creación de la instancia del socket
            display.setText("Socket Creado....\n");
            // creación de las instancias para el flujo de datos
            input = new DataInputStream(client.getInputStream());
            output = new DataOutputStream(client.getOutputStream());

            display.append("Enviando primer sumando\n");
            output.writeDouble(s1);
            display.append("Enviando segundo sumando\n");
            output.writeDouble(s2);
            display.append("El servidor dice....\n\n");
            suma = input.readDouble();
            Suma = String.valueOf(suma);
            display.append("El  resultado es: " + Suma + "\n\n");
            display.append("Cerrando cliente\n\n");
            client.close();
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public static void main(String args[])
    {
        new Cliente();

    }
}
