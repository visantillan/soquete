package com.tais;

import java.io.*;
import java.net.*;
import javax.swing.JTextArea;

public class ServidorHilo extends Thread
{

    JTextArea display;
    Socket conexion;
    ServerSocket server;
    DataInputStream input;
    DataOutputStream output;
    double s1, s2, suma;
    int clienteNum;

    public ServidorHilo(JTextArea d, ServerSocket s, Socket c, int n)
    {
        display = d;
        server = s;
        conexion = c;
        clienteNum = n;
        System.out.println("Se ha entrado al constructor del Hilo cliente: " + n);
    }

    public void run()
    {
        try
        {
            display.append("Se ha aceptado una conexión, para el cliente: " + clienteNum);
            display.append("\nRecibiendo sumados");
            input = new DataInputStream(conexion.getInputStream());
            output = new DataOutputStream(conexion.getOutputStream());

            display.append("\nRecibiendo el primer sumado. ");
            s1 = input.readDouble();
            display.append("\nRecibiendo el segundo sumado. ");
            s2 = input.readDouble();
            display.append("\nEnviando resultados..");
            suma = s1 + s2;
            output.writeDouble(suma);
            display.append("\nTransmisión terminada.\nSe cierra el socket del cliente\n");

            conexion.close();
        } catch (IOException e)
        {
            e.printStackTrace();
        }

    }
}
