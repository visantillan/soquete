package com.tais;

import com.tais.ServidorHilo;
import java.awt.*;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import javax.swing.*;
// paquete para los sockets

// PAQUETE PARA EL FLUJO DE DATOS
public class Servidor extends JFrame
{

    // declaración de los objectos sockets
    ServerSocket server;
    Socket s;
    int clienteNum = 0;
    JTextArea display;

    public Servidor()
    {
        super("Servidor");
        display = new JTextArea(20, 5);
        add("Center", display);
        setSize(400, 300);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }

    public void runServer()
    {
        try
        {
            InetAddress addr = InetAddress.getByName("192.168.175.86");
            // creación de la instancia socket para el cliente
            server = new ServerSocket(6000, 100 , addr);
            display.setText("Esperando por un cliente....\n\n");

            do
            {
                clienteNum++;
                s = server.accept();
                System.out.println("Atendiendo al cliente:  " + clienteNum);
                display.append("Socket cliente : " + s.toString());
                // creación de hilo
                new ServidorHilo(display, server, s, clienteNum).start();
            } while (s != null);

        } catch (IOException ex)
        {
        }
    }

    public static void main(String args[])
    {
        Servidor s = new Servidor();
        s.runServer();
    }
}
